# NamePros: Important #

This NamePros repository is **open source**.  NamePros developers should not add proprietary code here.

# Classes #

Exposed classes:

* \\Whois
* \\WhoisUtils (formerly \\utils)

In the examples, `WhoisUtils` is referred to as `utils`.  The examples also utilize `require`, which isn't necessary as long as Composer is used.